<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", nullable=false)
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $dropshipping;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $category;

    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $pricing = [];

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Inventory", mappedBy="product")
     */
    private $inventories;

    public function __construct()
    {
        $this->inventories = new ArrayCollection();
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     * @param mixed $reference
     * @return Product
     */
    public function setReference($reference): Product
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Product
     */
    public function setName($name): Product
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDropshipping(): ?string
    {
        return $this->dropshipping;
    }

    /**
     * @param mixed $dropshipping
     * @return Product
     */
    public function setDropshipping($dropshipping): Product
    {
        $this->dropshipping = $dropshipping;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory(): ?string
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     * @return Product
     */
    public function setCategory($category): Product
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Product
     */
    public function setDescription($description): Product
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPricing()
    {
        return $this->pricing;
    }

    /**
     * @param mixed $pricing
     * @return Product
     */
    public function setPricing($pricing): Product
    {
        $this->pricing = $pricing;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInventories()
    {
        return $this->inventories;
    }

    public function addInventory(Inventory $inventory)
    {
        if (!$this->inventories->contains($inventory)) {
            $this->inventories->add($inventory);
            $inventory->setProduct($this);
        }
    }

    public function removeInventory(Inventory $inventory)
    {
        if ($this->inventories->contains($inventory)) {
            $this->inventories->remove($inventory);
            $inventory->setProduct(null);
        }
    }

    /**
     * @param mixed $inventories
     * @return Product
     */
    public function setInventories($inventories): Product
    {
        $this->inventories = $inventories;

        return $this;
    }
}
