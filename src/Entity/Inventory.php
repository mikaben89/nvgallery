<?php

namespace App\Entity;

use App\Repository\InventoryRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InventoryRepository::class)
 */
class Inventory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="simple_array", length=255)
     */
    private $channels = [];

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $quantity = 0;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $inbounds = null;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="inventories")
     * @ORM\JoinColumn(name="reference", referencedColumnName="reference")
     */
    private $product;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getChannels(): array
    {
        return $this->channels;
    }

    /**
     * @param array $channels
     * @return Inventory
     */
    public function setChannels(array $channels): Inventory
    {
        $this->channels = $channels;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return Inventory
     */
    public function setQuantity(int $quantity): Inventory
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return array
     */
    public function getInbounds()
    {
        return json_decode($this->inbounds, true);
    }

    /**
     * @param array $inbounds
     * @return Inventory
     */
    public function setInbounds(array $inbounds): Inventory
    {
        $this->inbounds = json_encode($inbounds);

        return $this;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return Inventory
     */
    public function setProduct(Product $product): Inventory
    {
        $this->product = $product;

        return $this;
    }
}
