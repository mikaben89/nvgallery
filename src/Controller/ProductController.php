<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductController
 * @package App\Controller
 * @Route("/products")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("", name="products")
     */
    public function index(Request $request, ProductRepository  $repository, PaginatorInterface $paginator)
    {
        $reference = $request->query->get('reference');
        $data = $repository->findByReference($reference);
        $products = $paginator->paginate($data,  $request->query->getInt('page', 1), 15);

        return $this->render('product/index.html.twig', [
            'products' => $products,
            'reference' => $reference
        ]);
    }

    /**
     * @Route("/{reference}", name="show_product")
     */
    public function show(string $reference, ProductRepository  $repository)
    {
        $product = $repository->find($reference);
        if (!$product) {
            throw $this->createNotFoundException('The product does not exist');
        }

        return $this->render('product/show.html.twig', [
            'product' => $product
        ]);
    }

    /**
     * @Route("/search/{query}", name="search_product", methods={"GET"})
     */
    public function search(string $query, ProductRepository $productRepository)
    {
        $products = $productRepository->findByNameOrReference($query);

        return new JsonResponse($products, Response::HTTP_OK);
    }
}
