<?php

namespace App\Controller;

use App\Entity\Inventory;
use App\Repository\InventoryRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ERPInventoryController
 * @package App\Controller
 * @Route ("/erp")
 */
class ERPInventoryController extends AbstractController
{
    /**
     * @Route("/inventories", name="post_inventories", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param ProductRepository $productRepository,
     * @param InventoryRepository $inventoryRepository,
     * @return JsonResponse
     */
    public function bulkPost
    (
        Request $request,
        EntityManagerInterface $em,
        ProductRepository $productRepository,
        InventoryRepository $inventoryRepository
    ): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        foreach ($data as $inventoryPayload) {

            if(!isset($inventoryPayload['reference']) && null === $inventoryPayload['reference']) {
                continue;
            }

            $product = $productRepository->find($inventoryPayload['reference']);

            if (null === $product) {
                continue;
            }

            foreach ($inventoryPayload['inventories'] as $productInventory) {
                // Fetch existing inventory (same reference, channel and inbound)
                $inventory = $inventoryRepository->findOneByReferenceAndchannelsAndInbound(
                    $product->getReference(),
                    implode($productInventory['channels'], ','),
                    isset($productInventory['inbounds']) ?
                        json_encode($productInventory['inbounds'], JSON_UNESCAPED_SLASHES) :
                        null
                );

                if(null === $inventory) {
                    $inventory = new Inventory();
                }

                $inventory->setProduct($product);
                empty($productInventory['channels']) ? true : $inventory->setChannels($productInventory['channels']);
                empty($productInventory['quantity']) ? true : $inventory->setQuantity($productInventory['quantity']);
                empty($productInventory['inbounds']) ? true : $inventory->setInbounds($productInventory['inbounds']);

                $em->persist($inventory);
            }
        }
        $em->flush();

        return new JsonResponse([], Response::HTTP_NO_CONTENT);
    }
}
