<?php

namespace App\Controller;

use App\Entity\Product;
use App\Factory\ProductFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ERPProductController
 * @package App\Controller
 * @Route ("/erp")
 */
class ERPProductController extends AbstractController
{
    /**
     * @Route("/products", name="post_products", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function bulkPost(Request $request, EntityManagerInterface $em): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        foreach ($data as $productPayload) {
            if (null !== $em->getRepository(Product::class)->find($productPayload['reference'])) {
                continue;
            }
            $product = ProductFactory::fromArray($productPayload);
            $em->persist($product);
        }
        $em->flush();

        return new JsonResponse([], Response::HTTP_NO_CONTENT);
    }
}
