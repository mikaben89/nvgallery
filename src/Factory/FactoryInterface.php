<?php

namespace App\Factory;

interface FactoryInterface
{
    /**
     * @return mixed
     */
    public static function fromArray(array $array);
}