<?php

namespace App\Factory;

use App\Entity\Product;

class ProductFactory implements FactoryInterface
{
    /**
     * @param array $productArray
     * @return Product|mixed
     */
    public static function fromArray($productArray)
    {
        $product = new Product();
        empty($productArray['reference']) ? true : $product->setReference($productArray['reference']);
        empty($productArray['name']) ? true : $product->setName($productArray['name']);
        empty($productArray['dropshipping']) ? true : $product->setDropshipping($productArray['dropshipping']);
        empty($productArray['category']) ? true : $product->setCategory($productArray['category']);
        empty($productArray['description']) ? true : $product->setDescription($productArray['description']);
        empty($productArray['pricing']) ? true : $product->setPricing($productArray['pricing']);

        return $product;
    }
}