<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function findByReference($reference = null)
    {
        $qb = $this->createQueryBuilder('p');
        if($reference) {
            $qb->where('p.reference = :reference')
                ->setParameter('reference', $reference);
        }

        return $qb->getQuery()->getArrayResult();
    }
    public function findByNameOrReference($query)
    {
        $qb = $this->createQueryBuilder('p')
            ->where('p.name LIKE :query')
            ->orWhere('p.reference LIKE :query')
            ->setParameter('query', '%'.$query.'%');

        return $qb->getQuery()->getArrayResult();
    }
}
