<?php

namespace App\Repository;

use App\Entity\Inventory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class InventoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Inventory::class);
    }

    public function findOneByReferenceAndchannelsAndInbound($reference, $channels, $inbounds): ?Inventory
    {
        $qb = $this->createQueryBuilder('i')
            ->leftJoin('i.product', 'p')
            ->andWhere('p.reference = :reference')
            ->andWhere('i.channels = :channels')
            ->setParameter('reference', $reference)
            ->setParameter('channels', $channels);
        if (null === $inbounds) {
            $qb->andWhere('i.inbounds IS NULL');
        } else {
            $qb->andWhere('i.inbounds = :inbounds')
                ->setParameter('inbounds', $inbounds);
        }

        return $qb->getQuery()->getOneOrNullResult();
    }
}
