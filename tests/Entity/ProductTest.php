<?php

namespace App\Tests\Entity;

use App\Entity\Inventory;
use App\Entity\Product;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $product = new Product();
        $inventory = new Inventory();
        $product->setPricing('[{"price": 7682,"channel": "fr","vat_rate":0}]');
        $product->setDescription('desc');
        $product->setCategory('cat');
        $product->setDropshipping(true);
        $product->setName('name');
        $product->setReference('ref');
        $product->addInventory($inventory);

        $this->assertEquals('[{"price": 7682,"channel": "fr","vat_rate":0}]', $product->getPricing());
        $this->assertEquals('desc', $product->getDescription());
        $this->assertEquals('cat', $product->getCategory());
        $this->assertEquals(true, $product->getDropshipping());
        $this->assertEquals('name', $product->getName());
        $this->assertEquals('ref', $product->getReference());
        $this->assertEquals(new ArrayCollection([$inventory]), $product->getInventories());
    }
}
