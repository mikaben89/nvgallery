<?php

namespace App\Tests\Entity;

use App\Entity\Inventory;
use App\Entity\Product;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class InventoryTest extends TestCase
{
    public function testGettersAndSetters()
    {
        $inventory = new Inventory();
        $product = new Product();
        $inventory->setProduct($product);
        $inventory->setQuantity(10);
        $inventory->setChannels(['fr']);

        $this->assertEquals('10', $inventory->getQuantity());
        $this->assertEquals(['fr'], $inventory->getChannels());
        $this->assertEquals($product, $inventory->getProduct());
    }
}
