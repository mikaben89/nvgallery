# Readme

## Initialize containers
`cd docker && docker-compose build && docker-compose up -d`
### Inside php container
`composer install (better inside container php)` 
`sh bin/reinit.sh` 

## Routes ERP
`http://localhost:8080/erp/products` (POST)

`http://localhost:8080/erp/inventories` (POST)

## UI Products

`http://localhost:8080/products`

## Run unit tests (recommended inside container)
(in project root)
`bin/phpunit`
